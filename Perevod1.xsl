<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">


<xsl:template match="//fruit" mode="local">
        <tr>
            <td>
			<span style="color:green">
			<xsl:number value="position()" format="1. "/> 
                <xsl:value-of select="name"/>
				</span>
            </td>
            <td>
                <xsl:value-of select="price"/>
            </td>
        </tr>
    </xsl:template>
	
<xsl:template match="fruit"> 
        <xsl:if test="generate-id(.)=generate-id(//fruit[1])">

   <h1>The variety of fruits</h1>
   <table bgcolor="red" border="1">
        <xsl:apply-templates select="//fruit" mode="local"/>

    </table>
	        </xsl:if>
</xsl:template>



</xsl:stylesheet> 